
An overview : 

root dir : 	QmlRenderer/
			|
			| --- src/  : contains QmlRenderer Library code which does the rendering
			| --- lib/  : technically the build directory of QmlRenderer/src, contains .lib.so* files of QmlRenderer
			| --- test/ : contains unit tests and CLI executable of QmlRenderer Library
				  |
				  | --- tests/ 	: contains unit tests for root/QmlRenderer/src/ 
				  | --- run/	: contains CLI executable files
				  		|
				  		| --- exe/	: contains "QmlRender" - CLI executable file for QmlRenderer library 
				  		| --- src/	: contains source code of "QmlRender" 

				  		